﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class RagdollHelper : MonoBehaviour {

	[SerializeField]private OnRagDoll ORD;
	[SerializeField]private Rigidbody2D rb2d;
	public float ForceMultiplier=10f;
 
	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
	}
	
	void OnCollisionEnter2D(Collision2D col){
		Debug.Log ("asd");
		if (col.gameObject.tag == "Shell") {
			if (ORD.Status == PlayerState.Animated) {
				ORD.RagDollOn ();
				Rigidbody2D rigid = col.gameObject.GetComponent<Rigidbody2D> ();
				if (rigid) {
					Vector2 Force = rigid.velocity * rigid.mass*ForceMultiplier;
					Debug.Log (rigid.velocity.x);
					rb2d.AddForce (Force, ForceMode2D.Impulse);
				}

			}
			Destroy (col.gameObject);
		}
	}
}
