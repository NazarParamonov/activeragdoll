﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
	public bool IsFasingRight {  get; private set;} 
	[SerializeField] 
	private Transform target; 
	[SerializeField] 
	private Transform folowTarget; 
	[SerializeField] 
	private float MaxSpeed=5f;
	[SerializeField] 
	private Animator anim; 
	[SerializeField] 
	private OnRagDoll ragdoll; 
	void Start () {
		IsFasingRight=false;
	}

	void FixedUpdate () {
		int state = 0;
		if (ragdoll.Status == PlayerState.Animated) {
			if (Input.GetKey (KeyCode.A)) {
				Move (false);
				state = 1;
			}

			if (Input.GetKey (KeyCode.D)) {
				Move (true);
				state = 1;
			}
			
			anim.SetInteger ("State", state);
		}
		if (ragdoll.Status == PlayerState.Ragdolled) {
			if (Input.GetKey (KeyCode.Space)) {
				Vector3 newPosition = new Vector3 (folowTarget.position.x, target.position.y,target.position.z);
				target.position = newPosition;
				folowTarget.localPosition = Vector3.zero;
				ragdoll.RagDollOn ();
			}
		}
	}

	void Move(bool direct){
		float dir = 1;
		if (!IsFasingRight) {
			dir = -1;
		}
		if (IsFasingRight != direct) {
			Flip ();
		}

		target.position = Vector3.MoveTowards (target.position, target.position+ new Vector3(dir,0,0), MaxSpeed*Time.fixedDeltaTime);

	}
	void Flip(){
		if (IsFasingRight) {
			Vector3 NewScale = Vector3.one;
			NewScale.x = 1f;
			transform.localScale = NewScale;
			IsFasingRight = false;
		} else {
			Vector3 NewScale = Vector3.one;
			NewScale.x = -1f;
			transform.localScale = NewScale;
			IsFasingRight = true;
		}

	}
}
