﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnRagDoll : MonoBehaviour {
	private bool fl=true;
	public Animator anim;
	public GameObject [] ik;
	public Rigidbody2D [] allRigid;
	[SerializeField]
	public PlayerState Status { get; private set;} 

	void Start () {
		allRigid =GetComponentsInChildren<Rigidbody2D> ();
		RagDollOn ();
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.H)) {
			RagDollOn ();
		}
	}
	public void RagDollOn(){
		 
		foreach ( Rigidbody2D rb in allRigid ) {
			if (fl) {
				rb.velocity = Vector2.zero;
				rb.angularVelocity = 0;
				rb.bodyType = RigidbodyType2D.Kinematic;
			} else {
				rb.bodyType = RigidbodyType2D.Dynamic;
			}
		}
		anim.enabled = fl;
		foreach (GameObject g in ik) {
			g.SetActive (fl);
		}

		if (fl) {
			Status = PlayerState.Animated;
		} else {
			Status = PlayerState.Ragdolled;
		}
		fl = !fl;
	}

	public void AddRandomForce(){
		if (Status == PlayerState.Animated) {
			RagDollOn ();
		}
		int bone = Random.Range (0,allRigid.Length);
		Vector2 Force = new Vector2 (Random.Range(-50,50),Random.Range(-50,50));
		allRigid [bone].AddForce (Force,ForceMode2D.Impulse);
	}
}
