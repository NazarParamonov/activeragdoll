﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellSpawner : MonoBehaviour {
	public Vector2 direction;
	public bool Shoot =true;
	public float Force=10f;
	public float lifeTime = 4f;
	public float cd=1f;
	private float delay;
	[SerializeField]
	private GameObject ShellPrefab;
	// Use this for initialization
	void Start () {
		direction = Vector2.right;
		delay = cd;
	}
	
	// Update is called once per frame
	void Update () {
		

		delay -= Time.deltaTime;
		if (delay < 0) {
			delay = cd;
			Shot ();
		}

	}

	private void Shot(){
		var shell = Instantiate (ShellPrefab,transform.position, Quaternion.identity);
		var rb = shell.gameObject.GetComponent<Rigidbody2D> ();
		if (rb) {
			rb.AddForce (direction*Force,ForceMode2D.Impulse);
		}
		Destroy (shell.gameObject,lifeTime);
	}
}
